package com.amen.sda.concurrency;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Company {
	
	private ExecutorService service = Executors.newFixedThreadPool(7);
	
	public Company() {
		// TODO Auto-generated constructor stub
	}
	
	public void dodajNoweZlecenie(String imie_workera, long czas_wykonania){
		// stworzenie obiektu wykonywalnego
		Worker nowyPracownik = new Worker(imie_workera, czas_wykonania);
		
		// podanie obiektu wykonywalnego ( czyli takiego z metodą run ) do wątku
//		Thread watek = new Thread(nowyPracownik);
		
		// wystartowanie wątku (rozpoczyna pracę od metody run w Workerze )
//		watek.start();
		service.submit(nowyPracownik);
	}
}
