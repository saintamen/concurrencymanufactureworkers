package com.amen.sda.concurrency;

public class Worker implements Runnable {

	private String imie;
	private long czas_czekania;
	private static Object synchronizowany_obiekt = new Object();

	public Worker(String imie, long czas_czekania) {
		super();
		this.imie = imie;
		this.czas_czekania = czas_czekania;
	}

	@Override
	public void run() {
		wykonaj();
		
		synchronized(synchronizowany_obiekt){
			System.out.println(imie + " rozpoczyna prace.");
	
			try {
				Thread.sleep(czas_czekania);
			} catch (InterruptedException e) {
				// wywoła się w przypadku gdy inny wątek przerwie jego działanie
				e.printStackTrace();
			}
	
			System.out.println(imie + " zakonczyl prace.");
		}
		
	}
	
	public void wykonaj(){
		synchronized(synchronizowany_obiekt){
			System.out.println(imie + " sruuu prace.");
			try {
				Thread.sleep(czas_czekania);
			} catch (InterruptedException e) {
				// wywoła się w przypadku gdy inny wątek przerwie jego działanie
				e.printStackTrace();
			}
			System.out.println(imie + " opuszcza sruuu prace.");
		}
	}
}
