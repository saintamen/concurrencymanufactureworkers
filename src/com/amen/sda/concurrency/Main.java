package com.amen.sda.concurrency;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Company f = new Company();
		

		Scanner sc = new Scanner(System.in);
		while(sc.hasNextLine()){
			String line = sc.nextLine();
			String[] splits = line.split(" ");
			
			// splits[0] - imie pracownika
			// splits[1] - czas pracy
			f.dodajNoweZlecenie(splits[0], Long.parseLong(splits[1]));
		}
	}

}
